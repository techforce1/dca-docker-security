# -*- mode: ruby -*-
# # vi: set ft=ruby :

# Specify minimum Vagrant version and Vagrant API version
Vagrant.require_version '>= 1.6.0'
VAGRANTFILE_API_VERSION = '2'
# Require JSON module
require 'json'

# Read YAML file with box details
servers = JSON.parse(File.read(File.join(File.dirname(__FILE__), 'servers.json')))

$script = <<-SCRIPT
echo "#custom hosts rules" >> /etc/hosts
echo "192.168.56.111 node1" >> /etc/hosts
echo "192.168.56.112 node2 example.com notary-server" >> /etc/hosts

echo 'alias notary="notary --server https://example.com --trustDir ~/.docker/trust"' >> ~/.bashrc
echo 'alias notary="notary --server https://example.com --trustDir ~/.docker/trust"' >> /home/vagrant/.bashrc

export DEBIAN_FRONTEND=noninteractive
apt-get update
apt-get remove docker docker-engine docker.io containerd runc
apt-get install \
    apt-transport-https \
    ca-certificates \
    curl \
    software-properties-common

export DOCKER_EE_URL="<ee-url-here>"
export DOCKER_EE_VERSION=18.09
curl -fsSL "${DOCKER_EE_URL}/ubuntu/gpg" | sudo apt-key add -

add-apt-repository \
   "deb [arch=$(dpkg --print-architecture)] $DOCKER_EE_URL/ubuntu \
   $(lsb_release -cs) \
   stable-$DOCKER_EE_VERSION"

apt-get update
apt-get install docker-ee docker-ee-cli containerd.io -y

usermod -aG docker vagrant
SCRIPT

# Create boxes
Vagrant.configure(VAGRANTFILE_API_VERSION) do |config|

  # Iterate through entries in JSON file
  servers.each do |server|
    config.vm.define server['name'] do |srv|
      srv.vm.box = server['box']
      srv.vm.hostname = server['name']
      srv.vm.network 'private_network', ip: server['ip_addr']
      srv.vm.provider :virtualbox do |v|
    	  v.memory = server['ram']
    	  v.cpus = server['vcpu']
      end # srv.vm.provider
      config.vm.provision "shell", inline: $script, privileged: true
    end # config.vm.define
  end # servers.each
end # Vagrant.configure
