THEME=moon
HIGHLIGHT_STYLE=zenburn
.PHONY: all

all: overview.html

overview.html:
	pandoc -t revealjs \
		--highlight-style=${HIGHLIGHT_STYLE} \
		-V width=1920 \
		-V height=1080 \
		-V theme=${THEME} \
		--standalone \
		-V revealjs-url=https://revealjs.com \
		-o slides.html \
		slides.md
