# docker container run --rm -it --name ucp \
#  -v /vagrant/docker_subscription.lic:/config/docker_subscription.lic \
#  -v /var/run/docker.sock:/var/run/docker.sock \
#  docker/ucp:3.1.5 install \
#  --admin-username admin \
#  --admin-password test1234 \
#  --host-address 192.168.56.111 \
#  --pod-cidr 192.168.56.0/27 \
#  --interactive \
#  --debug
docker container run --rm  -it --name ucp \
  -v /vagrant/docker_subscription.lic:/config/docker_subscription.lic \
	-v /var/run/docker.sock:/var/run/docker.sock docker/ucp:2.2.18 install \
	--host-address 192.168.56.111 \
	--interactive \
	--san node1 \
	--admin-username admin \
	--admin-password test1234
