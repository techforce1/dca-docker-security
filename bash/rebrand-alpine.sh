#!/usr/bin/env bash

docker pull alpine:latest
docker tag alpine:latest example.com/admin/alpine:1
# Sign and push the image to DTR
export DOCKER_CONTENT_TRUST=1
docker push example.com/admin/alpine:1
