#!/usr/bin/env bash

if [[ $EUID -ne 0 ]]; then
   echo "This script must be run as root" 1>&2
else
  for i in {1..2};do
    export DOMAIN_NAME=192.168.56.11${i}
    export TCP_PORT=443
    openssl s_client -connect $DOMAIN_NAME:$TCP_PORT -showcerts </dev/null 2>/dev/null | openssl x509 -outform PEM | tee /usr/local/share/ca-certificates/$DOMAIN_NAME.crt
    update-ca-certificates
  done
  service docker restart
fi
