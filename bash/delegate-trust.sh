#!/usr/bin/env bash

# Delegate trust, and add that public key with the role targets/releases
notary delegation add --publish \
  example.com/admin/alpine targets/releases \
  -https://192.168.56.112
# Delegate trust, and add that public key with the role targets/admin
notary delegation add --publish \
  example.com/admin/alpine targets/admin \
  --all-paths /vagrant/temp/cert.pem
