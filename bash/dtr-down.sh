#!/usr/bin/env bash

docker run -it --rm docker/dtr destroy \
  --ucp-username admin \
  --ucp-password test1234 \
  --ucp-url https://192.168.56.111 \
  --ucp-insecure-tls
