#!/usr/bin/env bash

if [[ $EUID -ne 0 ]]; then
   echo "This script must be run as root" 1>&2
else
  if [ "$(hostname)" != 'node1' ];then
    echo "run on node1 please"
  else
    curl -L https://github.com/theupdateframework/notary/releases/download/v0.6.1/notary-Linux-amd64 -o /usr/local/bin/notary
    chmod +x /usr/local/bin/notary
  fi
fi
