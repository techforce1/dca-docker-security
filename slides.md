# roadmap
## roadmap
* Describe the process of signing an image
* Demonstrate that an image passes a security scan
* Enable Docker Content Trust
* Configure RBAC in UCP
* Integrate UCP with LDAP/AD
* Demonstrate creation of UCP client bundles
* Describe default engine security
* Describe swarm default security
* Describe MTLS
* Identity roles
* Describe the difference between UCP workers and managers
* Describe process to use external certificates with UCP and DTR

## Enterprise edition
![enterprise == clicker](img/gui.png "lol")


# Set up UPC

## universal controll plane
controlls everything docker related in your swarm

new version deploy seems broken

upgrade from 2.2.18 to 3.0 and from 3.0 to latest works, but slow

## Create environment
```bash
vagrant up
```

## run the docker installer image for ucp
```bash
vagrant ssh node1
docker container run --rm  -it --name ucp \
  -v /vagrant/docker_subscription.lic:/config/docker_subscription.lic \
	-v /var/run/docker.sock:/var/run/docker.sock docker/ucp:2.2.18 install \
	--host-address 192.168.56.111 \
	--interactive \
	--san node1 \
	--admin-username admin \
	--admin-password test1234
```

## Or the easy way
```bash
vagrant ssh node1
cd /vagrant/bash/
./ucp-up.sh
```

## wait

![Zzzzz Zzzzz](img/netflix.jpeg "lol")

## login
https://192.168.56.111

user: admin

password: test1234

copy add worker node
```bash
#looks like:
docker swarm join --token SWMTKN-1-1aetwcej314j2jehnq8j5fjtzelam3w-3c9mmg3aot5ca25ozadpw3phf 192.168.56.111:2377
```

## Add node
A.K.A. a crappy swarm
```bash
vagrant ssh node2
#paste command (shift insert)
```
#Set up DTR


## Docker Trusted registry


## setup (the hard way)
```bash
vagrant ssh node2
docker run -it --rm docker/dtr install \
	--ucp-node node2 \
	--ucp-username admin \
	--ucp-password test1234 \
	--ucp-url https://192.168.56.111 \
	--ucp-insecure-tls
```

## Or the easy way
```bash
vagrant ssh node2
cd /vagrant/bash/
./dtr-up.sh
```

## wait (again)

![Zzzzz Zzzzz](img/netflix.jpeg "lol")

## some configs for later

https://192.168.56.112 > System > Security > enable scanning

[online sync]

# ssl ca chains

## load the ca of ucp and dtr into the host
```bash
vagrant ssh node1
sudo -i
openssl s_client -connect $DOMAIN_NAME:$TCP_PORT \
  -showcerts </dev/null 2>/dev/null \
  | openssl x509 -outform PEM \
  | tee /usr/local/share/ca-certificates/$DOMAIN_NAME.crt
#or just:
cd /vagrant/bash
./ssl.sh
```
repeat for node2

## login to dtr
```bash
vagrant ssh node1
docker login example.com
#admin:test1234
```
repeat for node2

#notary

## notary
helps the docker daemon to manage the signing process

## setup
```bash
vagrant ssh node1
sudo -i
cd /vagrant/bash/
./notary.sh
exit
```
## verify install
```bash
notary version
#notary
# Version:    0.6.1
# Git commit: d6e1431f

type notary
#notary is aliased to notary --server https://example.com --trustDir ~/.docker/trust
```

## new client bundle
https://192.168.56.111 > admin > My Profile > client bundle

[+ new client bundle]

extract to temp/

```bash
notary key import /vagrant/temp/key.pem
```
#Enable Docker Content Trust

## enable DCT
https://192.168.56.111 > Admin > Admin settings > Docker Content Trust

check checkbox! > save

## verify DCT

```bash
#docker service create --name helloworld 1234loltest4321/alpine ping docker.com
vagrant ssh node1
cd /vagrant/bash/
. ./hello-world-fail.sh
#failed to resolve image digest using content trust: Error: remote trust data does not exist for
#docker.io/1234loltest4321/alpine: notary.docker.io does not have trust data for
#docker.io/1234loltest4321/alpine
```


#Describe the process of signing an image
## create repo
https://192.168.56.112 > repositorys > [new repository]

admin / alpine > [create]

## pull and rebrand alpine
```bash
vagrant ssh node1
docker pull alpine:latest
docker tag alpine:latest example.com/admin/alpine:1
# Sign and push the image to DTR
export DOCKER_CONTENT_TRUST=1
docker push example.com/admin/alpine:1

# or the fast version (note the . at the start)
. /vagrant/bash/rebrand-alpine.sh
```

root key > repo key

## verify signed image
```bash
#docker service create --name pingtest example.com/admin/alpine:1 ping docker.com
vagrant ssh node1
cd /vagrant/bash
./hello-world-succes.sh
```

## for the ui fans
https://192.168.56.111/manage/resources/services

#Demonstrate that an image passes a security scan
## enable security scanner
https://192.168.56.112 > System > Security > enable
[Online]

## enable security scan in repo
https://192.168.56.112 > Repositories > admin/alpine > Settings

[on push]

https://192.168.56.112 > Repositories > admin/alpine > Tags

[scan now]

#Configure RBAC in UCP

## roles
![Roles and rights](img/rbac.png "lol")

#Integrate UCP with LDAP/AD
## LDAP intergration via gui
https://192.168.56.111 > admin > admin settings > Authentication & Authorization

#Demonstrate creation of UCP client bundles
## create new bundle
https://192.168.56.111 > admin > My Profile > Client bundles
[+ new client bundle]

#Describe default engine security
## items
* trust for images
* tls for docker daemon
* protect registry with client side certificates

## security
[https://docs.docker.com/engine/security/security/](https://docs.docker.com/engine/security/security/)

* deny all “mount” operations;
* deny access to raw sockets (to prevent packet spoofing);
* deny access to some filesystem operations, like creating new device nodes, changing the owner of files, or altering attributes (including the immutable flag);
* deny module loading;

Apparmor / SeLinux

#Describe swarm default security

#Describe MTLS
## multi Tls?...
[https://docs.docker.com/engine/swarm/how-swarm-mode-works/pki/](https://docs.docker.com/engine/swarm/how-swarm-mode-works/pki/)
fun read about TLS Cert rotation:
[https://diogomonica.com/2017/01/11/hitless-tls-certificate-rotation-in-go/](https://diogomonica.com/2017/01/11/hitless-tls-certificate-rotation-in-go/)

#Identity roles
## roles
![Roles](img/identity.svg "lol")

#Describe the difference between UCP workers and managers
## managers
manage
## workers
work

## recap
manager:

* maintain cluster
* schedule services

workers:

* execute containers

#Describe process to use external certificates with UCP and DTR
click then click and click some more
